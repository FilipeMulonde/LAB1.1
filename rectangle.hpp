#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "shape.hpp"
#include "base-types.hpp"

class Rectangle :public Shape{
public:
	Rectangle(double centerX, double centerY, double width, double height);
	double getArea() const;
	rectangle_t getFrame() const;
	void move(double dx, double dy);
	void move(point_t p);
	void show() const;

private:
	rectangle_t rectangle_;
};

#endif
