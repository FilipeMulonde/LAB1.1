#include "circle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

Circle::Circle(double centerX, double centerY, double radius) :center_({ centerX, centerY }), radius_(radius) {}
double Circle::getArea() const
{
	return radius_*radius_*M_PI;
}
rectangle_t Circle::getFrame() const
{
	return rectangle_t{ center_, radius_ * 2, radius_ * 2 };
}
void Circle::move(double dx, double dy)
{
	center_.x += dx;
	center_.y += dy;
}
void Circle::move(point_t p)
{
	center_ = p;
}

void Circle::show() const
{
	std::cout << "Area of Circle: " << Circle::getArea() << std::endl;
	rectangle_t frame = Circle::getFrame();
	std::cout << "Frame X : " << frame.pos.x << "Frame Y: " << frame.pos.y << std::endl;
	std::cout << "Frame -Width: " << frame.width << "Frame Height: " << frame.height << "\n\n";
}
