#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "shape.hpp"
#include "base-types.hpp"

int main()
{
	std::cout << "Making rectangle x:11, y:13, W:5, h:5" << std::endl;
	Rectangle rectangle(11, 13, 5, 5);
	rectangle.show();

	std::cout << "moving the rectangle at the point DX=5, dy=10" << std::endl;
	rectangle.move(5, 10);
	rectangle.show();

	std::cout << "Move the rectangle to a point x:20.41, y:13.11" << std::endl;
	rectangle.move({ 20.41, 13.11 });
	rectangle.show();

	std::cout << "The creation of a circle X in a:15 d:19 p:4" << std::endl;
	Circle circle (15, 19, 4);
	circle.show();

	std::cout << "Move the circle to the point x:12.41, g:20" << std::endl;
	circle .move({ 12.41, 20 });
	circle .show();

	std::cout << "Creating a triangle point a(11,30), B(55,6), C(45,36)" << std::endl;
	Triangle triangle({ 11,30 }, { 55,6 }, { 45,36 });
	triangle.show();

	std::cout << "Move the triangle DH : 2.9, dy : 6" << std::endl;
	triangle.move(1.8, 5);
	triangle.show();

	return 0;
}
